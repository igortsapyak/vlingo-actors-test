// Copyright © 2012-2018 Vaughn Vernon. All rights reserved.
//
// This Source Code Form is subject to the terms of the
// Mozilla Public License, v. 2.0. If a copy of the MPL
// was not distributed with this file, You can obtain
// one at https://mozilla.org/MPL/2.0/.

package playground;

import io.vlingo.actors.Definition;
import io.vlingo.actors.World;
import org.junit.Test;
import playground.playground.Actors.ContactKeeperActor;
import playground.playground.Actors.CreditCheckerActor;
import playground.playground.Actors.CustomerVaultActor;
import playground.playground.Actors.ProcessStep;
import playground.playground.Actors.ServicePlannerActor;
import playground.playground.dto.Processor;
import playground.playground.dto.RegistrationData;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;

public class PlaygroundTest {

    @Test
    public void test() throws InterruptedException {
        final World world = World.startWithDefaults("playground");
        CountDownLatch countDownLatch = new CountDownLatch(4);
        ProcessStep step1 = world.actorFor(Definition.has(CustomerVaultActor.class, Definition.parameters(countDownLatch), "create_customer"), ProcessStep.class);
        ProcessStep step2 = world.actorFor(Definition.has(ContactKeeperActor.class, Definition.parameters(countDownLatch), "set_up_contact_info"), ProcessStep.class);
        ProcessStep step3 = world.actorFor(Definition.has(ServicePlannerActor.class, Definition.parameters(countDownLatch), "select_service_plan"), ProcessStep.class);
        ProcessStep step4 = world.actorFor(Definition.has(CreditCheckerActor.class, Definition.parameters(countDownLatch), "check_credit"), ProcessStep.class);

        Processor processor = new Processor(new LinkedList<>(Arrays.asList(step1, step2, step3, step4)), new RegistrationData(null, null, null));

        processor.next();
        countDownLatch.await();

        world.terminate();
    }

}
