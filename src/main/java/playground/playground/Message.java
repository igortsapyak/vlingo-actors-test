package playground.playground;

import lombok.Value;
import playground.playground.dto.Processor;
import playground.playground.dto.RegistrationData;

@Value
public class Message {

    private RegistrationData registrationData;
    private Processor process;

}
