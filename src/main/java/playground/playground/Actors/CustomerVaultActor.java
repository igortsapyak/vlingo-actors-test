package playground.playground.Actors;


import playground.playground.dto.RegistrationData;

import java.util.concurrent.CountDownLatch;

public class CustomerVaultActor extends AbstractProcessStep {

    public CustomerVaultActor(CountDownLatch countDownLatch) {
        super(countDownLatch);
    }

    @Override
    public void perform(RegistrationData data) {
        System.out.println(String.format("CustomerVault: handling creating customer... %s", data));
    }
}
