package playground.playground.Actors;

import io.vlingo.actors.Stoppable;
import playground.playground.Message;

public interface ProcessStep extends Stoppable {
    void receive(Message message);

}
