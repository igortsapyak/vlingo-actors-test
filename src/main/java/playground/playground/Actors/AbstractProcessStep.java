package playground.playground.Actors;

import io.vlingo.actors.Actor;
import lombok.AllArgsConstructor;
import playground.playground.Message;
import playground.playground.dto.RegistrationData;


import java.util.concurrent.CountDownLatch;

@AllArgsConstructor
public abstract class AbstractProcessStep extends Actor implements ProcessStep {
    private CountDownLatch countDownLatch;


    @Override
    public void receive(Message message) {
        perform(message.getRegistrationData());
        message.getProcess().next();
        countDownLatch.countDown();
    }

    protected abstract void perform(RegistrationData data);


    @Override
    protected void afterStop() {
        System.out.println("Process dead: " + this.toString());
    }
}
