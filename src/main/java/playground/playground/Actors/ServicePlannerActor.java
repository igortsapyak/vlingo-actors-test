package playground.playground.Actors;


import playground.playground.dto.RegistrationData;

import java.util.concurrent.CountDownLatch;

public class ServicePlannerActor extends AbstractProcessStep {

    public ServicePlannerActor(CountDownLatch countDownLatch) {
        super(countDownLatch);
    }

    @Override
    public void perform(RegistrationData data) {
        System.out.println(String.format("ServicePlanner: handling creating customer... to plan a new customer service, %s", data));
    }
}
