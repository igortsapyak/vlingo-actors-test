package playground.playground.Actors;


import playground.playground.dto.RegistrationData;

import java.util.concurrent.CountDownLatch;

public class CreditCheckerActor extends AbstractProcessStep {

    public CreditCheckerActor(CountDownLatch countDownLatch) {
        super(countDownLatch);
    }

    @Override
    public void perform(RegistrationData data) {
        System.out.println(String.format("CreditChecker: handling creating customer... to perform credit check, %s", data));
    }
}
