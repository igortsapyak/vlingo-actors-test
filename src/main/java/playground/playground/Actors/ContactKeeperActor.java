package playground.playground.Actors;

import lombok.SneakyThrows;
import playground.playground.dto.RegistrationData;

import java.util.concurrent.CountDownLatch;

public class ContactKeeperActor extends AbstractProcessStep {

    public ContactKeeperActor(CountDownLatch countDownLatch) {
        super(countDownLatch);
    }

    @SneakyThrows
    @Override
    public void perform(RegistrationData data) {
        System.out.println(String.format("ContactKeeper: handling creating customer... to keep contact information, %s", data));
    }
}
