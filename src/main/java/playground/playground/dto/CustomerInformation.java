package playground.playground.dto;

import lombok.Value;

@Value
public class CustomerInformation {

    private String name;
    private String federalTaxId;

}
