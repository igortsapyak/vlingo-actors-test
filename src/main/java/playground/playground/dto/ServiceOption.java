package playground.playground.dto;

import lombok.Value;

@Value
public class ServiceOption {

    private String id;
    private String description;

}
