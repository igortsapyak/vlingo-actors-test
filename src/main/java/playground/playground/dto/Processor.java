package playground.playground.dto;

import lombok.AllArgsConstructor;
import playground.playground.Actors.ProcessStep;
import playground.playground.Message;

import java.util.Queue;

import static java.util.Objects.nonNull;

@AllArgsConstructor
public class Processor {
    private Queue<ProcessStep> steps;
    private RegistrationData registrationData;
//    private CountDownLatch countDownLatch;

    public void next() {
        ProcessStep currentStep = steps.poll();
        if (nonNull(currentStep)) {
            currentStep.receive(new Message(registrationData, this));
        }

    }


}
