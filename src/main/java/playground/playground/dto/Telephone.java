package playground.playground.dto;

import lombok.Value;

@Value
public class Telephone {

    private String number;

}
