package playground.playground.dto;

import lombok.Value;

@Value
public class RegistrationData {

    private CustomerInformation customerInformation;
    private ContactInformation contactInformation;
    private ServiceOption serviceOption;

}
