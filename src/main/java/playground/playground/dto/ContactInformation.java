package playground.playground.dto;

import lombok.Value;

@Value
public class ContactInformation {

    private PostalAddress postalAddress;
    private Telephone telephone;
}
