package playground.playground.dto;

import lombok.Value;

@Value
public class PostalAddress {
    private String address;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
}
